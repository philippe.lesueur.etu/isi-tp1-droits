# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Lesueur, Philippe, philippe.lesueur.etu@univ-lille.fr

- Nom, Prénom, email: ___

## Question 1

Si le fichier titi.txt appartient à toto (toto est le "user"), alors il ne pourra pas écrire dessus car c'est le premier triplet qui sera utilisé.
Si le fichier titi.txt n'appartient pas à toto mais à son groupe (ubuntu), alors il pourra écrire dessus car le second triplet sera utilisé.
S'il nappartient ni à l'un, ni à l'autre, alors le troisième sera utilisé et il ne pourra pas écrire dessus.

## Question 2

Pour un répertoire, x signifie que l'on pêut accéder à son contenu. toto ne peut donc pas accéder au contenu puisqu'il appartient au groupe ubuntu.
Lorsque l'on essaye de lister le contenu de mydir, on obtient la liste des fichier mais on a pas les informations qui leur sont liées.
Cela s'explique par le fait que nous pouvons lire le répertoire --> accès à la liste des fichiers. Mais nous n'avons pas accès à ces fichiers,
nous ne pouvons donc pas obtenir d'informations sur eux.

## Question 3

Réponse pour la première tentative de lecture :
"""
RUID : 1001
RGID : 1001
EUID : 1001
EGID : 1001
Cannot open mydata.txt: Permission denied
"""
1001 correspond à l'id de l'utilisateur toto.

Réponse après avoir fait "chmod u+s suid":
"""
RUID : 1001
RGID : 1001
EUID : 1000
EGID : 1001
File opens successfully
"""
1000 correspond à l'id de l'utilisateur ubuntu.

## Question 4

"""
EUID :  1001
EGID :  1001
"""
L'EUID n'a pas changé.

## Question 5

La commande chfn permet de changer les informations de l'utilisateur, notamment le fullname.
"-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn"

"toto:x:1001:1001:,,,:/home/toto:/bin/bash"

"toto:x:1001:1001:,7,3630,288488:/home/toto:/bin/bash"

## Question 6

Les mots de passe sont stockés dans le fichier etc/shadow pour que seul l'administrateur
puisse modifier le fichier et que seuls les membre du groupe shadow puissent lire les informations.

## Question 7

Pour cette que question, les droits des dir_a et dir_b doivent être drwsrwxr-T et doivent avoir été créés par ubuntu.
Ici, le T signifie que les utilisateurs "other" ne peuvent pas exécuter le dossier et que celui-ci ne peut être supprimé que par le user.
Ainsi, les membres du groupe pourront créer et supprimer tous les fichiers qu'ils veulent (on ajoute admin aux groupes a et b).
Lors de la création de fichier, ceux-ci  appartiendront au groupe de leur créateur et ceux qui n'ont pas créé le fichier
utiliseront le triplet other qui ne leur permettra pas d'effacer ni renommer ce fichier. 
Le fichier dir_c est créé par ubuntu mais doit appartenir au groupe admin. Les permissions de dir_c sont drwxrwxr-x.
De ce fait, seul ubuntu peut modifier les éléments du dossier.

Si l'on veut que tous les membres du groupe a (ou réciproquement, b) puissent avoir les mêmes droits dans dir_a, on fait chmod g+s.
Les fichiers créés prendront alors groupe_a comme groupe et le second triplet sera utilisé.

## Question 8

Les droits de l'exécutable doivent être -rwxr-xr-x.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








