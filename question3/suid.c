#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	FILE *f;
    if (argc != 2){
		printf("On doit avoir le chemin du fichier à lire en argument.");
	}
	else{
		printf("RUID : %d\n", getuid());
		printf("RGID : %d\n", getgid());
		printf("EUID : %d\n", geteuid());
		printf("EGID : %d\n", getegid());
		f = fopen(argv[1], "r");
		if(f==NULL){
			perror("Cannot open file");
			exit(EXIT_FAILURE);
		}
		else{
			printf("File opens successfully");
			fclose(f);
		}
	}
    return 0;
}
